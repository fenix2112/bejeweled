package {

import com.config.GameConfig;
import com.view.main.ViewMain;


import flash.display.Sprite;

import flash.display.StageAlign;
import flash.display.StageScaleMode;
import flash.events.Event;

import robotlegs.bender.bundles.mvcs.MVCSBundle;
import robotlegs.bender.extensions.contextView.ContextView;
import robotlegs.bender.extensions.signalCommandMap.SignalCommandMapExtension;

import robotlegs.bender.framework.impl.Context;


[SWF(width="900", height="600", frameRate="30", backgroundColor="#ffffff")]
public class Bejeweled extends Sprite {

    private var _context:Context;
    private var _main:ViewMain;

    public function Bejeweled() {
        super();
        stage.align = StageAlign.TOP_LEFT;
        stage.scaleMode = StageScaleMode.NO_SCALE;
        stage.addEventListener(Event.ENTER_FRAME, _handlerAddedToStage, false, 0, true);
    }


    protected function _initialize():void{
        _robotlegsStart();
        _childrenAdd();
    }


    private function _robotlegsStart ():void {
        trace(">>>_robotlegsStart")
        _context = new Context()
        _context.install(MVCSBundle, SignalCommandMapExtension);

        _context.configure(GameConfig, this);
        _context.configure(new ContextView(this));

    }

    private function _childrenAdd():void{
        _main = new ViewMain();
        addChild(_main);
    }

    private function _handlerAddedToStage(event:Event):void{
        stage.removeEventListener(Event.ENTER_FRAME, _handlerAddedToStage, false);
        _initialize();
    }

}
}
