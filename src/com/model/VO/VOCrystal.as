/**
 * Created with IntelliJ IDEA.
 * User: helga
 * Date: 05.03.14
 * Time: 8:09
 * To change this template use File | Settings | File Templates.
 */
package com.model.VO {
public class VOCrystal {

    private var _type:int;
    private var _posX:Number;
    private var _posY:Number;
    private var _indexX:int;
    private var _indexY:int;


    public function VOCrystal(pType:int = 0, pPosX:Number = 0, pPosY:Number = 0, pIndexX:int = 0, pIndexY:int = 0) {
        _type = pType;
        _posX = pPosX;
        _posY = pPosY;
        _indexX = pIndexX;
        _indexY = pIndexY;
    }

    public function get type():int {
        return _type;
    }

    public function set type(value:int):void {
        _type = value;
    }

    public function get posX():Number {
        return _posX;
    }

    public function set posX(value:Number):void {
        _posX = value;
    }

    public function get posY():Number {
        return _posY;
    }

    public function set posY(value:Number):void {
        _posY = value;
    }

    public function get indexX():int {
        return _indexX;
    }

    public function set indexX(value:int):void {
        _indexX = value;
    }

    public function get indexY():int {
        return _indexY;
    }

    public function set indexY(value:int):void {
        _indexY = value;
    }
}
}
