/**
 * Created with IntelliJ IDEA.
 * User: helga
 * Date: 07.03.14
 * Time: 11:03
 * To change this template use File | Settings | File Templates.
 */
package com.model.VO {
import flash.display.Sprite;

public class VOScreenNavigator {

    private var _name:String;
    private var _screen:Sprite;

    public function VOScreenNavigator(pName:String, pScreen:Sprite) {
        _name = pName;
        _screen = pScreen;
    }

    public function get name():String {
        return _name;
    }

    public function set name(value:String):void {
        _name = value;
    }

    public function get screen():Sprite {
        return _screen;
    }

    public function set screen(value:Sprite):void {
        _screen = value;
    }
}
}
