/**
 * Created with IntelliJ IDEA.
 * User: helga
 * Date: 04.03.14
 * Time: 14:52
 * To change this template use File | Settings | File Templates.
 */
package com.config {
import com.signals.SignalStartGame;
import com.signals.SignalStopGame;
import com.view.main.MediatorViewMain;
import com.view.main.ViewMain;
import com.view.viewGame.MediatorViewGame;
import com.view.viewGame.ViewGame;
import com.view.viewScore.MediatorViewScore;
import com.view.viewScore.ViewScore;
import com.view.viewStart.MediatorViewStart;
import com.view.viewStart.ViewStart;

import flash.events.IEventDispatcher;

import robotlegs.bender.extensions.contextView.ContextView;
import robotlegs.bender.extensions.mediatorMap.api.IMediatorMap;
import robotlegs.bender.extensions.signalCommandMap.api.ISignalCommandMap;
import robotlegs.bender.framework.api.IConfig;
import robotlegs.bender.framework.api.IContext;
import robotlegs.bender.framework.api.IInjector;

public class GameConfig implements IConfig{
//--------------------------------------------------------------------------------------------------------- 
    // 
    //  PUBLIC & INTERNAL VARIABLES 
    // 
    //---------------------------------------------------------------------------------------------------------
    [Inject]
    public var context:IContext;

    [Inject]
    public var mediatorMap:IMediatorMap;

    [Inject]
    public var injector:IInjector;

    [Inject]
    public var commandMap:ISignalCommandMap;

    [Inject]
    public var contextView:ContextView;

    [Inject]
    public var dispatcher:IEventDispatcher;

    //--------------------------------------------------------------------------------------------------------- 
    //
    // PRIVATE & PROTECTED VARIABLES
    //
    //---------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------------- 
    //
    //  CONSTRUCTOR 
    // 
    //---------------------------------------------------------------------------------------------------------
    public function GameConfig() {
        trace(">>>> GameConfig")
    }

    //--------------------------------------------------------------------------------------------------------- 
    // 
    //  GETTERS & SETTERS   
    // 
    //---------------------------------------------------------------------------------------------------------

    //--------------------------------------------------------------------------------------------------------- 
    // 
    //  PUBLIC & INTERNAL METHODS 
    // 
    //---------------------------------------------------------------------------------------------------------
    public function configure():void {
        trace(">>>> configure")

        // Map commands
       // commandMap.map(SignalStartGame).SomeCommand...

        // Map views
        mediatorMap.map(ViewMain).toMediator(MediatorViewMain);
        mediatorMap.map(ViewStart).toMediator(MediatorViewStart);
        mediatorMap.map(ViewGame).toMediator(MediatorViewGame);
        mediatorMap.map(ViewScore).toMediator(MediatorViewScore);

        // Map independent notification signals
        injector.map(SignalStartGame).asSingleton();
        injector.map(SignalStopGame).asSingleton();



        // Start
        context.afterInitializing(_initialize);

    }
    //---------------------------------------------------------------------------------------------------------
    //
    // PRIVATE & PROTECTED METHODS 
    //
    //---------------------------------------------------------------------------------------------------------
    private function _initialize():void {

        trace(">>>> initialized")
    }

    //--------------------------------------------------------------------------------------------------------- 
    // 
    //  EVENT HANDLERS  
    // 
    //---------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------------- 
    // 
    //  END OF CLASS
    // 
    //---------------------------------------------------------------------------------------------------------
}
}
