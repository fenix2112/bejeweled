/**
 * Created with IntelliJ IDEA.
 * User: helga
 * Date: 04.03.14
 * Time: 14:42
 * To change this template use File | Settings | File Templates.
 */
package com.view.viewGame {
import com.greensock.TweenLite;
import com.model.VO.VOCrystal;
import com.view.ViewAbstract;
import com.view.components.cell.CellItemRenderer;
import com.view.components.clock.Clock;
import com.view.components.crystals.Crystal;

import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.utils.clearInterval;
import flash.utils.setInterval;

import org.osflash.signals.Signal;


public class ViewGame extends ViewAbstract {
    //---------------------------------------------------------------------------------------------------------
    // 
    //  PUBLIC & INTERNAL VARIABLES 
    // 
    //---------------------------------------------------------------------------------------------------------
    public var siglanGameOver:Signal = new Signal();
    //--------------------------------------------------------------------------------------------------------- 
    //
    // PRIVATE & PROTECTED VARIABLES
    //
    //---------------------------------------------------------------------------------------------------------
    private const _MAX_CRYSTAL_NUM:int = 5;
    private const _MAX_CELLS_X:int = 10;
    private const _MAX_CELLS_Y:int = 10;

    private var _containerCrystals:Sprite;
    private var _containerGrid:Sprite;

    private var _crystal1:Crystal;
    private var _crystal2:Crystal;

    private var _crystalsInGame:Array = new Array();
    private var _crystalsToRemove:Array = new Array();
    private var _crystalsNew:Array = new Array();
    private var _crystalsMoves:Array = new Array();

    private var _userPoints:int = 0;

    private var _intervalShowNewCrystals:int = -1;
    private var _intervalCheckCoincidence:int = -1;

    private var _gameTimer:Clock;
    //---------------------------------------------------------------------------------------------------------
    //
    //  CONSTRUCTOR 
    // 
    //---------------------------------------------------------------------------------------------------------
    public function ViewGame() {
        super();
    }

    //--------------------------------------------------------------------------------------------------------- 
    // 
    //  GETTERS & SETTERS   
    // 
    //---------------------------------------------------------------------------------------------------------

    //--------------------------------------------------------------------------------------------------------- 
    // 
    //  PUBLIC & INTERNAL METHODS 
    // 
    //---------------------------------------------------------------------------------------------------------
    public function startGame():void{
        _gameTimer.start();
    }
    //--------------------------------------------------------------------------------------------------------- 
    //
    // PRIVATE & PROTECTED METHODS 
    //
    //---------------------------------------------------------------------------------------------------------
    override protected function _initialize():void{
        _containerGrid = new Sprite();
        _containerCrystals = new Sprite();
        addChild(_containerGrid);
        addChild(_containerCrystals);

        _gameTimer = new Clock();
        addChild(_gameTimer);
        _gameTimer.signalTimeOver.addOnce(_handleGameOver)

        _gridCreate();
        _crystalsAdd();
    }



    /**
     * create the grid field for crystals
     * */
    private function _gridCreate():void{
        var pRow:int = 0;

        for (var i:int = 0; i < _MAX_CELLS_X * _MAX_CELLS_Y; i++) {
            var pCell:CellItemRenderer = new CellItemRenderer();
            _containerGrid.addChild(pCell);

            if(i/(pRow+1) >= _MAX_CELLS_X ){
                pRow ++;
            }

            pCell.x = pCell.width * (i - (pRow * (_MAX_CELLS_X)));
            pCell.y = pCell.height * pRow;
        }

        _containerGrid.x = stage.stageWidth / 2 - _containerGrid.width / 2;
        _containerGrid.y = stage.stageHeight / 2 - _containerGrid.height / 2;
    }



    /**
     * create and add crystals
     * */
    private function _crystalsAdd():void{
        var pRow:int = 0;

        //add crystals

        //------------------------------
        //   create the array of crystals. They should be set in the correct order.
        //   There should be no case when 3 crystals are near each other in row or column.
        //------------------------------
        _crystalsInGame = new Array();
        for (var p:int = 0; p < _MAX_CELLS_X; p++) {
            _crystalsInGame[p] = new Array();


            //------------------------------
            //   make sure that there will be no repetitions of the same crystals in rows
            //------------------------------
            var pRowRepeat:Array = new Array();
            for (var t:int = 0; t <_MAX_CELLS_Y; t++) {
                var pCrystal:Crystal = new Crystal();
                _containerCrystals.addChild(pCrystal);

                var pStoneNum:int = Math.random()*_MAX_CRYSTAL_NUM;//3


                // detect and change repeated volumes in array row (max repeat num = 3)
                pRowRepeat.push(pStoneNum)
                if(pRowRepeat.length > 2){
                    var pPrev:int = pRowRepeat[pRowRepeat.length-2];
                    while(pStoneNum == pPrev){
                        pStoneNum =Math.random() * _MAX_CRYSTAL_NUM;
                    }
                    pRowRepeat[pRowRepeat.length-1] = pStoneNum;
                    pRowRepeat.shift();
                }

                //------------------------------
                //   make sure that there will be no repetitions of the same crystals in columns
                //------------------------------
                 if(p > 1){
                     var pUpperCrystal:Crystal = Crystal(_crystalsInGame[p-1][t]);
                     if(pUpperCrystal.crystalVO.type == pStoneNum){
                         var pPrevColumn:Crystal = Crystal(_crystalsInGame[p][t - 1])
                         if(pPrevColumn){
                             while((pStoneNum == pUpperCrystal.crystalVO.type) || (pStoneNum ==pPrevColumn.crystalVO.type)){
                                 pStoneNum =Math.random() * _MAX_CRYSTAL_NUM;
                             }
                         }else{
                             while(pStoneNum == pUpperCrystal.crystalVO.type){
                                 pStoneNum =Math.random() * _MAX_CRYSTAL_NUM;
                             }
                         }
                    }
                 }

                var pCrystalVO:VOCrystal = new VOCrystal();
                pCrystalVO.type = pStoneNum;
                pCrystalVO.indexX = t;
                pCrystalVO.indexY = p;

                pCrystal.crystalVO = pCrystalVO;

                _crystalsInGame[p][t] = pCrystal;
            }
        }

        _traceArray();

        // set to correct place and add info to VOCrystal of each crystal item
        for (var i:int = 0; i < _MAX_CELLS_X * _MAX_CELLS_Y; i++) {
            var pCrystal:Crystal = Crystal(_containerCrystals.getChildAt(i));

            if(i/(pRow+1) >= _MAX_CELLS_X ){
                pRow ++;
            }

            pCrystal.x = pCrystal.width * (i - (pRow * (_MAX_CELLS_X)));
            pCrystal.y = pCrystal.height * pRow;

            pCrystal.crystalVO.posX =  pCrystal.x;
            pCrystal.crystalVO.posY =  pCrystal.y;
            pCrystal.addEventListener(MouseEvent.CLICK, _handlerMouseClick, false, 0, true);
        }

        _containerCrystals.x = stage.stageWidth / 2 - _containerCrystals.width / 2;
        _containerCrystals.y = stage.stageHeight / 2 - _containerCrystals.height / 2;


    }

    /**
     * change data in the crystals VO and change their places
     * */
    private function _swapCrystals():void{
        TweenLite.to(_crystal1,.5, {x:_crystal2.crystalVO.posX, y:_crystal2.crystalVO.posY});
        TweenLite.to(_crystal2,.5, {x:_crystal1.crystalVO.posX, y:_crystal1.crystalVO.posY, onComplete:_updateGame});

        var pCrystal1VO:VOCrystal = new VOCrystal(_crystal1.crystalVO.type,
                                                  _crystal2.crystalVO.posX,
                                                  _crystal2.crystalVO.posY,
                                                  _crystal2.crystalVO.indexX,
                                                  _crystal2.crystalVO.indexY);

        var pCrystal2VO:VOCrystal = new VOCrystal(_crystal2.crystalVO.type,
                                                  _crystal1.crystalVO.posX,
                                                  _crystal1.crystalVO.posY,
                                                  _crystal1.crystalVO.indexX,
                                                  _crystal1.crystalVO.indexY);

        _crystalsInGame[_crystal1.crystalVO.indexY][_crystal1.crystalVO.indexX] = _crystal2;
        _crystalsInGame[_crystal2.crystalVO.indexY][_crystal2.crystalVO.indexX] = _crystal1;

        _crystal1.crystalVO = pCrystal1VO;
        _crystal2.crystalVO = pCrystal2VO;

        _crystal1.pTextField.text = _crystal1.crystalVO.type.toString();
        _crystal2.pTextField.text = _crystal2.crystalVO.type.toString();
    }


    /**
     * update the view of the game field
     * */
    private function _updateGame():void{
        _crystal1.isSelected = false;
        _crystal2.isSelected = false;
        _crystal1 = _crystal2 = null;

        _checkCoincidence();
    }




    /**
     * Check coincidence, if there are 3 crystals in row or in column
     * */
    private function _checkCoincidence():void{
        if(_intervalCheckCoincidence != -1){
            clearInterval(_intervalCheckCoincidence);
        }
        _crystal1 = null;
        _crystal2 = null;

        _traceArray();

        // create a new array _crystalsToRemove for a new round
        _crystalsToRemove = new Array();

        // remove selection
        for (var p:int = 0;  p < _crystalsInGame.length; p++) {//_MAX_CELLS_X
            for (var f:int = 0;  f < _crystalsInGame[0].length; f++) {     //_MAX_CELLS_Y
                var object:Crystal = Crystal(_crystalsInGame[p][f]);
                object.isSelected = false;
            }
        }

        var pRepeat:Array;
        //------------------------------
        //   check row
        //------------------------------
        for (var t:int = 0; t < _MAX_CELLS_X; t++) {
            pRepeat = new Array();
            for (var w:int = 0; w < _MAX_CELLS_Y; w++) {
                var pCrystal:Crystal = Crystal(_crystalsInGame[t][w]);
                if(pRepeat.length ==0){
                    pRepeat.push(Crystal(_crystalsInGame[t][w]));
                }else{
                    var pLastInRepeat:Crystal = Crystal(pRepeat[pRepeat.length -1]);
                    if(pLastInRepeat.crystalVO.type == pCrystal.crystalVO.type){
                        pRepeat.push(pCrystal);

                        if(pRepeat.length > 2){
                            for (var i:int = 0; i < pRepeat.length; i++) {
                                if(_crystalsToRemove.indexOf(pRepeat[i]) == -1){
                                    _crystalsToRemove.push(pRepeat[i]);
                                }
                            }
                        }
                    }else{

                        // save items that were found in _crystalsToRemove array if their amount is more then 2
                        if(pRepeat.length > 2){
                            for (var i:int = 0; i < pRepeat.length; i++) {
                                if(_crystalsToRemove.indexOf(pRepeat[i]) == -1){
                                    _crystalsToRemove.push(pRepeat[i]);
                                }
                            }
                        }
                        pRepeat = new Array();
                        pRepeat.push(Crystal(_crystalsInGame[t][w]));
                    }
                }
            }
        }

        //------------------------------
        //   check column
        //------------------------------
        for (var a:int = 0; a < _MAX_CELLS_X; a++) {
            pRepeat = new Array();

            for (var b:int = 0; b < _MAX_CELLS_Y; b++) {
                if(pRepeat.length ==0){
                    pRepeat.push(Crystal(_crystalsInGame[b][a]));
                }else{
                    var pCrystal:Crystal = Crystal(_crystalsInGame[b][a]);
                    var pLastInCells:Crystal = Crystal(pRepeat[pRepeat.length -1]);
                    if(pLastInCells.crystalVO.type == pCrystal.crystalVO.type){
                        pRepeat.push(pCrystal);
                    }else{

                        // save items that were found in _crystalsToRemove array if their amount is more then 2
                        if(pRepeat.length > 2){
                            for (var i:int = 0; i < pRepeat.length; i++) {
                                if(_crystalsToRemove.indexOf(pRepeat[i]) == -1){
                                    _crystalsToRemove.push(pRepeat[i]);
                                }

                            }
                        }
                        pRepeat = new Array();
                        pRepeat.push(Crystal(_crystalsInGame[b][a]));
                    }
                }
            }

            if(pRepeat.length > 2){
                for (var i:int = 0; i < pRepeat.length; i++) {
                    if(_crystalsToRemove.indexOf(pRepeat[i]) == -1){
                        _crystalsToRemove.push(pRepeat[i]);
                    }
                }
            }

        }
        trace("length " + _crystalsToRemove.length)

        for (var i:int = 0; i < _crystalsToRemove.length; i++) {
            var object1:Crystal = Crystal(_crystalsToRemove[i]);
            trace(object1.crystalVO.indexX + " ||  " + object1.crystalVO.indexY + " --> "+object1.crystalVO.type)

        }

        // remove selected crystal
       _crystalsSelectedRemove();

    }



    /**
     * move down crystals
     * */
    private function _crystalsMoveDown():void{
        var pCrystalMoveArray:Array = new Array();

        // remove deleted crystals from the array
        for (var i:int = 0; i < _crystalsToRemove.length; i++) {
            var pOldCrystal:Crystal =  Crystal(_crystalsToRemove[i]);
            _crystalsInGame[pOldCrystal.crystalVO.indexY][pOldCrystal.crystalVO.indexX] = null;
        }

        // update data for crystals that are left
        for (var t:int = _crystalsInGame.length -1 ; t != -1; t--) {

            var pIndexCoef:int = 0;

            for (var w:int = _crystalsInGame[0].length-2; w != -1 ; w--){

                var pCrystal:Crystal = Crystal(_crystalsInGame[w][t]);
                var pCrystalPrev:Crystal = Crystal(_crystalsInGame[w+1][t]);

                if(!pCrystalPrev){
                    pIndexCoef ++;
                }

                if(pCrystal && (pCrystalPrev == null)){
                    pCrystal.crystalVO.indexY = w + pIndexCoef;
                    pCrystal.crystalVO.posY = pCrystal.crystalVO.indexY * pCrystal.height;
                    _crystalsInGame[w + pIndexCoef][t] = pCrystal;
                    _crystalsInGame[w][t] = null;
                    pCrystalMoveArray.push(pCrystal);
                    pIndexCoef --;
                }
            }
        }


        // set motion
        for (var i:int = 0; i < pCrystalMoveArray.length; i++) {
            var pCrystal:Crystal = Crystal(pCrystalMoveArray[i]);
            TweenLite.to(pCrystal,.5, {y:pCrystal.crystalVO.posY});
        }

        _newCrystalIndexesGenerate();
    }

    /**
     * generate new crystals
     * */
    private function _newCrystalIndexesGenerate():void{
        for (var i:int = 0; i < _MAX_CELLS_X; i++) {
            for (var t:int = 0; t < _MAX_CELLS_Y; t++) {
                var pCrystal:Crystal =  Crystal(_crystalsInGame[i][t]);
                if(!pCrystal){
                    var pCrystal:Crystal = new Crystal();
                    _containerCrystals.addChild(pCrystal);

                    var pPosX:Number = t * pCrystal.width;
                    var pPosY:Number = i * pCrystal.height;
                    pCrystal.crystalVO = new VOCrystal(Math.random() * _MAX_CRYSTAL_NUM, pPosX, pPosY, t, i);

                    pCrystal.y = -500;
                    pCrystal.x = pCrystal.crystalVO.posX;
                    pCrystal.addEventListener(MouseEvent.CLICK, _handlerMouseClick, false, 0, true);

                    _crystalsInGame[i][t] = pCrystal;
                    _crystalsNew.push(pCrystal);
                    trace("new " + pCrystal.crystalVO.indexX + " " + pCrystal.crystalVO.indexY + " || " + pCrystal.crystalVO.type)
                }
            }
        }
        trace("update: " + _crystalsToRemove.length + " "+_crystalsNew.length)

        for (var r:int = 0; r < _crystalsInGame.length; r++) {
            var pString:String = '';
            var pArray:Array = _crystalsInGame[r] as Array;
            for (var f:int = 0; f < pArray.length; f++) {
                var object:Crystal = Crystal(pArray[f]);
                pString += object.crystalVO.indexX +"  " + object.crystalVO.indexY + "  |"
            }
            trace(pString);
        }

        _intervalShowNewCrystals = setInterval(_newCrystalsShow, 500);
    }


    /**
     * remove selected crystals
     * */
    private function _crystalsSelectedRemove():void{
        _userPoints += _crystalsToRemove.length;
        for (var i:int = 0; i < _crystalsToRemove.length; i++) {
            var pCrystal:Crystal = Crystal(_crystalsToRemove[i]);
            pCrystal.removeEventListener(MouseEvent.CLICK, _handlerMouseClick, false);
            _containerCrystals.removeChild(pCrystal);

        }

        _crystalsMoveDown();

        _traceArray();
    }




   /**
    * show new crystals
    * */
    private function _newCrystalsShow(){
       clearInterval(_intervalShowNewCrystals);
       _intervalShowNewCrystals = -1;

        for (var i:int = 0; i < _crystalsNew.length; i++) {
            var pCrystal:Crystal = Crystal(_crystalsNew[i]);
            TweenLite.to(pCrystal,.5, {x:pCrystal.crystalVO.posX, y:pCrystal.crystalVO.posY});
        }

        _crystalsNew = new Array();

       if(_crystalsToRemove.length > 0){
           _intervalCheckCoincidence = setInterval(_checkCoincidence,.5);
       }else{
           var pIsMovePossible:Boolean = _movePossibleCheck();

           trace("Moves are possible " + pIsMovePossible);
           if(!pIsMovePossible){
               _handleGameOver();
           }

       }

    }


    private function _movePossibleCheck():Boolean{
        var pIsPossible:Boolean = false;

        var pPairsInRow:Array = new Array();
        var pPairsInColumns:Array = new Array();


        var pRepeat:Array;
        //------------------------------
        //   check row
        //------------------------------
        for (var t:int = 0; t < _MAX_CELLS_X; t++) {
            pRepeat = new Array();
            for (var w:int = 0; w < _MAX_CELLS_Y; w++) {
                var pCrystal:Crystal = Crystal(_crystalsInGame[t][w]);

                if(pRepeat.length ==0){
                    pRepeat.push(Crystal(_crystalsInGame[t][w]));
                }else {
                    var pLastInRepeat:Crystal = Crystal(pRepeat[pRepeat.length -1]);
                    if(pLastInRepeat.crystalVO.type == pCrystal.crystalVO.type){
                        if(pRepeat.indexOf(pCrystal) == -1){
                            pRepeat.push(pCrystal);
                        }
                    }else{
                        if(pRepeat.length >= 2){
                            pPairsInRow.push(pRepeat);
                        }

                        pRepeat = new Array();
                        pRepeat.push(Crystal(_crystalsInGame[t][w]));
                    }
                }
            }
        }


        trace("--------------  ROW MOVES ")
        for (var i:int = 0; i < pPairsInRow.length; i++) {
            var pString:String = '';
            var pPairs:Array = pPairsInRow[i];
            for (var j:int = 0; j < pPairs.length; j++) {
                var pCr:Crystal = pPairs[j];
                pString +=  pCr.crystalVO.type;
            }
            trace(pString)

        }

        //------------------------------
        //   check column
        //------------------------------
        for (var a:int = 0; a < _MAX_CELLS_X; a++) {
            pRepeat = new Array();

            for (var b:int = 0; b < _MAX_CELLS_Y; b++) {
                var pCrystal:Crystal = Crystal(_crystalsInGame[b][a]);

                if(pRepeat.length ==0){
                    pRepeat.push(Crystal(_crystalsInGame[b][a]));
                }else{
                    var pLastInCells:Crystal = Crystal(pRepeat[pRepeat.length -1]);
                    if(pLastInCells.crystalVO.type == pCrystal.crystalVO.type){
                        if(pRepeat.indexOf(pCrystal) == -1){
                            pRepeat.push(pCrystal);
                        }
                    }else{

                        if(pRepeat.length >= 2){
                            pPairsInColumns.push(pRepeat);
                        }

                        pRepeat = new Array();
                        pRepeat.push(Crystal(_crystalsInGame[b][a]));
                    }
                }
            }

        }

        trace("-------------- CELL MOVES")
        for (var i:int = 0; i < pPairsInColumns.length; i++) {
            var pString:String = '';
            var pPairs:Array = pPairsInColumns[i];
            for (var j:int = 0; j < pPairs.length; j++) {
                var pCr:Crystal = pPairs[j];
                pString +=  pCr.crystalVO.type;
            }
            trace(pString)
        }





        // check if the are crystals with the same color in row one step from selected pair
        for (var i:int = 0; i < pPairsInRow.length; i++) {
            var pRepeats:Array = pPairsInRow[i];
            var pCrystal1:Crystal = Crystal(pRepeats[0]);
            var pCrystal2:Crystal = Crystal(pRepeats[pRepeats.length - 1]);
            if(pCrystal1.crystalVO.indexX > 1){
                var pPrev:Crystal =  _crystalsInGame[pCrystal1.crystalVO.indexX-2][pCrystal1.crystalVO.indexY];
                if(pPrev.crystalVO.type == pCrystal1.crystalVO.type){
                    pIsPossible = true;
                    return pIsPossible;
                }
            }
            if(pCrystal2.crystalVO.indexX < (_crystalsInGame[0] as Array).length -2){
                var pNext:Crystal =  _crystalsInGame[pCrystal2.crystalVO.indexX+2][pCrystal2.crystalVO.indexY];
                if(pNext.crystalVO.type == pCrystal1.crystalVO.type){
                    pIsPossible = true;
                    return pIsPossible;
                }
            }
        }
        // check if the are crystals with the same color in Cell one step from selected pair
        for (var i:int = 0; i < pPairsInColumns.length; i++) {
            var pRepeats:Array = pPairsInColumns[i];
            var pCrystal1:Crystal = Crystal(pRepeats[0]);
            var pCrystal2:Crystal = Crystal(pRepeats[pRepeats.length - 1]);

            if(pCrystal1.crystalVO.indexY > 1){
                var pPrev:Crystal =  _crystalsInGame[pCrystal1.crystalVO.indexX][pCrystal1.crystalVO.indexY - 2];
                if(pPrev){
                    if(pPrev.crystalVO.type == pCrystal1.crystalVO.type){
                        pIsPossible = true;
                        return pIsPossible;
                    }
                }
            }

            if(pCrystal2.crystalVO.indexY < _crystalsInGame.length -2){
                var pNext:Crystal =  _crystalsInGame[pCrystal2.crystalVO.indexX][pCrystal2.crystalVO.indexY + 2];
                if(pNext){
                    if(pNext.crystalVO.type == pCrystal1.crystalVO.type){
                        pIsPossible = true;
                        return pIsPossible;
                    }
                }
            }

        }



        return pIsPossible;
    }


    /**
     * trace array with crystals
     * */
    private function _traceArray():void{
        trace("---------------------")
        for (var y:int = 0; y < _MAX_CELLS_X; y++) {
            var pTest:Array =_crystalsInGame[y];
            var pString:String = '';
            for (var i:int = 0; i < pTest.length; i++) {
                var object:Crystal = Crystal(pTest[i]);
                if(object){
                    var pType:int = object.crystalVO.type;
                }else{
                    pType = -2;
                }

                pString += pType.toString()+" ";
            }
            trace("!!>> :  "+ pString);
        }
        trace("---------------------")

    }
    //---------------------------------------------------------------------------------------------------------
    // 
    //  EVENT HANDLERS  
    // 
    //---------------------------------------------------------------------------------------------------------
    private function _handlerMouseClick(event:MouseEvent):void{
        if(!_crystal1){
            _crystal1 = Crystal(event.currentTarget);
            _crystal1.isSelected = true;
            trace("cr1 " + _crystal1.crystalVO.indexX + " " + _crystal1.crystalVO.indexY)
        }else{
           var pCrystal:Crystal = Crystal(event.currentTarget);
            trace("cr2 " + pCrystal.crystalVO.indexX + " " + pCrystal.crystalVO.indexY)

           if(pCrystal == _crystal1){
               _crystal1.isSelected = false;
               _crystal1 = null;
               return;
           }

           if(pCrystal.crystalVO.indexX == _crystal1.crystalVO.indexX-1 || pCrystal.crystalVO.indexX == _crystal1.crystalVO.indexX+1){
               if( pCrystal.crystalVO.indexY == _crystal1.crystalVO.indexY){
                   _crystal2 = pCrystal;
                   _crystal2.isSelected = true;
                   _swapCrystals();
               }
           }else if(pCrystal.crystalVO.indexY == _crystal1.crystalVO.indexY-1 || pCrystal.crystalVO.indexY == _crystal1.crystalVO.indexY+1){
               if( pCrystal.crystalVO.indexX == _crystal1.crystalVO.indexX){
                   _crystal2 = pCrystal;
                   _crystal2.isSelected = true;
                   _swapCrystals();
               }
           }
        }
    }


    private function _handleGameOver():void{
        siglanGameOver.dispatch(_userPoints);
    }
    //---------------------------------------------------------------------------------------------------------
    // 
    //  END OF CLASS
    // 
    //---------------------------------------------------------------------------------------------------------
}
}
