/**
 * Created with IntelliJ IDEA.
 * User: helga
 * Date: 07.03.14
 * Time: 10:23
 * To change this template use File | Settings | File Templates.
 */
package com.view.viewGame {
import com.signals.SignalStartGame;
import com.signals.SignalStopGame;

import robotlegs.bender.bundles.mvcs.Mediator;

public class MediatorViewGame extends Mediator {
//--------------------------------------------------------------------------------------------------------- 
    // 
    //  PUBLIC & INTERNAL VARIABLES 
    // 
    //---------------------------------------------------------------------------------------------------------
    [Inject]
    public var view:ViewGame;

    [Inject]
    public var signalStop:SignalStopGame = new SignalStopGame();

    [Inject]
    public var signalStart:SignalStartGame;
    //--------------------------------------------------------------------------------------------------------- 
    //
    // PRIVATE & PROTECTED VARIABLES
    //
    //---------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------------- 
    //
    //  CONSTRUCTOR 
    // 
    //---------------------------------------------------------------------------------------------------------
    public function MediatorViewGame() {
        super();
    }

    //--------------------------------------------------------------------------------------------------------- 
    // 
    //  GETTERS & SETTERS   
    // 
    //---------------------------------------------------------------------------------------------------------

    //--------------------------------------------------------------------------------------------------------- 
    // 
    //  PUBLIC & INTERNAL METHODS 
    // 
    //---------------------------------------------------------------------------------------------------------
    override public function initialize():void {
        super.initialize();
        view.siglanGameOver.add(_handlerGameOver);
        signalStart.add(_handlerStartGame);
    }

    override public function destroy():void{
    }
    //--------------------------------------------------------------------------------------------------------- 
    //
    // PRIVATE & PROTECTED METHODS 
    //
    //---------------------------------------------------------------------------------------------------------
     private function _handlerStartGame():void{
         view.startGame();
     }
    //--------------------------------------------------------------------------------------------------------- 
    // 
    //  EVENT HANDLERS  
    // 
    //---------------------------------------------------------------------------------------------------------
    private function _handlerGameOver(pScore:int):void{
        trace("SCORE " + pScore);
        signalStop.dispatch(pScore);
    }
    //--------------------------------------------------------------------------------------------------------- 
    // 
    //  END OF CLASS
    // 
    //---------------------------------------------------------------------------------------------------------
}
}
