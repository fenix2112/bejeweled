/**
 * Created with IntelliJ IDEA.
 * User: helga
 * Date: 04.03.14
 * Time: 15:32
 * To change this template use File | Settings | File Templates.
 */
package com.view.main {
import com.greensock.TweenLite;
import com.model.VO.VOScreenNavigator;
import com.view.ViewAbstract;
import com.view.viewGame.ViewGame;
import com.view.viewScore.ViewScore;
import com.view.viewStart.ViewStart;

import flash.display.Sprite;

public class ViewMain extends ViewAbstract {
    //---------------------------------------------------------------------------------------------------------
    // 
    //  PUBLIC & INTERNAL VARIABLES 
    // 
    //---------------------------------------------------------------------------------------------------------

    //--------------------------------------------------------------------------------------------------------- 
    //
    // PRIVATE & PROTECTED VARIABLES
    //
    //---------------------------------------------------------------------------------------------------------
    private const _START:String = "start_game";
    private const _PLAY:String = "play_game";
    private const _SCORE:String = "show_score";

    private var _viewStart:ViewStart  = new ViewStart();;
    private var _viewGame:ViewGame = new ViewGame();
    private var _viewScore:ViewScore = new ViewScore();

    private var _container:Vector.<VOScreenNavigator> = new Vector.<VOScreenNavigator>();
    private var _currentScreen:String = _START;
    //---------------------------------------------------------------------------------------------------------
    //
    //  CONSTRUCTOR 
    // 
    //---------------------------------------------------------------------------------------------------------
    public function ViewMain() {
        super();
    }

    //--------------------------------------------------------------------------------------------------------- 
    // 
    //  GETTERS & SETTERS   
    // 
    //---------------------------------------------------------------------------------------------------------

    //--------------------------------------------------------------------------------------------------------- 
    // 
    //  PUBLIC & INTERNAL METHODS 
    // 
    //---------------------------------------------------------------------------------------------------------
    public function gameShow():void{
        _currentScreen = _PLAY;
        _hidePrevScreen();
     }

    public function scoreShow():void{
        _currentScreen = _SCORE;
        _hidePrevScreen();
    }
    //--------------------------------------------------------------------------------------------------------- 
    //
    // PRIVATE & PROTECTED METHODS 
    //
    //---------------------------------------------------------------------------------------------------------
     override protected function _initialize():void{
         trace(">>> ViewMain");
         _viewGame.visible = false;
         _viewScore.visible = false;

         addChild(_viewStart);
         addChild(_viewGame);
         addChild(_viewScore);

         _container.push(new VOScreenNavigator(_START, _viewStart));
         _container.push(new VOScreenNavigator(_PLAY, _viewGame));
         _container.push(new VOScreenNavigator(_SCORE, _viewScore));
     }



    /**
     * remove previous screen
     * */
     private function _hidePrevScreen():void{

         for (var i:int = 0; i < _container.length; i++) {
             var pScreenVO:VOScreenNavigator = VOScreenNavigator(_container[i]);

             if(pScreenVO.screen.visible && pScreenVO.name != _currentScreen){
                 TweenLite.to(pScreenVO.screen,1, {alpha:0, onComplete: _showCurrentScreen, onCompleteParams:[pScreenVO.screen]});
             }
         }
     }

    /**
     * show current screen
     * */
    private function _showCurrentScreen(pScreen:Sprite):void{
        pScreen.visible = false;

        for (var i:int = 0; i < _container.length; i++) {
            var pScreenVO:VOScreenNavigator = VOScreenNavigator(_container[i]);
            if(pScreenVO.name == _currentScreen){
                pScreenVO.screen.visible = true;
                pScreenVO.screen.alpha = 0;
                TweenLite.to(pScreenVO.screen,1, {alpha:1});
            }
        }
    }

    //---------------------------------------------------------------------------------------------------------
    // 
    //  EVENT HANDLERS  
    // 
    //---------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------------- 
    // 
    //  END OF CLASS
    // 
    //---------------------------------------------------------------------------------------------------------
}
}

