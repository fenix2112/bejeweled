/**
 * Created with IntelliJ IDEA.
 * User: helga
 * Date: 04.03.14
 * Time: 16:29
 * To change this template use File | Settings | File Templates.
 */
package com.view.components.cell {
import com.view.ViewAbstract;

import mx.core.BitmapAsset;

public class CellItemRenderer extends ViewAbstract {
//--------------------------------------------------------------------------------------------------------- 
    // 
    //  PUBLIC & INTERNAL VARIABLES 
    // 
    //---------------------------------------------------------------------------------------------------------

    //--------------------------------------------------------------------------------------------------------- 
    //
    // PRIVATE & PROTECTED VARIABLES
    //
    //---------------------------------------------------------------------------------------------------------
    [Embed (source="/../src/assets/images/square.png")]
    private var Square:Class;

    private var _cellImage:BitmapAsset;
    //--------------------------------------------------------------------------------------------------------- 
    //
    //  CONSTRUCTOR 
    // 
    //---------------------------------------------------------------------------------------------------------
    public function CellItemRenderer() {
        super();
    }

    //--------------------------------------------------------------------------------------------------------- 
    // 
    //  GETTERS & SETTERS   
    // 
    //---------------------------------------------------------------------------------------------------------

    //--------------------------------------------------------------------------------------------------------- 
    // 
    //  PUBLIC & INTERNAL METHODS 
    // 
    //---------------------------------------------------------------------------------------------------------

    //--------------------------------------------------------------------------------------------------------- 
    //              F
    // PRIVATE & PROTECTED METHODS 
    //
    //---------------------------------------------------------------------------------------------------------
      override protected function _initialize():void{
          _cellImage = new  Square();
          addChild(_cellImage);
      }
    //--------------------------------------------------------------------------------------------------------- 
    // 
    //  EVENT HANDLERS  
    // 
    //---------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------------- 
    // 
    //  END OF CLASS
    // 
    //---------------------------------------------------------------------------------------------------------
}
}
