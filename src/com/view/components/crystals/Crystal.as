/**
 * Created with IntelliJ IDEA.
 * User: helga
 * Date: 04.03.14
 * Time: 23:50
 * To change this template use File | Settings | File Templates.
 */
package com.view.components.crystals {
import com.model.VO.VOCrystal;
import com.view.ViewAbstract;

import flash.display.Bitmap;
import flash.display.Sprite;
import flash.text.TextField;
import flash.text.TextFormat;

import mx.core.FlexTextField;

public class Crystal extends ViewAbstract {
//--------------------------------------------------------------------------------------------------------- 
    // 
    //  PUBLIC & INTERNAL VARIABLES 
    // 
    //---------------------------------------------------------------------------------------------------------

    //--------------------------------------------------------------------------------------------------------- 
    //
    // PRIVATE & PROTECTED VARIABLES
    //
    //---------------------------------------------------------------------------------------------------------
    [Embed(source="/../src/assets/images/stone1.png")]
    private var Stone1:Class;

    [Embed(source="/../src/assets/images/stone2.png")]
    private var Stone2:Class;

    [Embed(source="/../src/assets/images/stone3.png")]
    private var Stone3:Class;

    [Embed(source="/../src/assets/images/stone4.png")]
    private var Stone4:Class;

    [Embed(source="/../src/assets/images/stone5.png")]
    private var Stone5:Class;

    private var _stone:Sprite;
    private var _background:Sprite;
    private var _crystalVO:VOCrystal;

    private var _isSelected:Boolean = false;
    public var  pTextField:TextField;

    //---------------------------------------------------------------------------------------------------------
    //
    //  CONSTRUCTOR 
    // 
    //---------------------------------------------------------------------------------------------------------
    public function Crystal() {
        super();
    }

    //--------------------------------------------------------------------------------------------------------- 
    // 
    //  GETTERS & SETTERS   
    // 
    //---------------------------------------------------------------------------------------------------------
    public function get crystalVO():VOCrystal {
        return _crystalVO;
    }

    public function set crystalVO(value:VOCrystal):void {
        _crystalVO = value;
        _crystalImageAdd();
    }


    public function set isSelected(value:Boolean):void {
        _isSelected = value;
         _background.visible = _isSelected;
    }
    //--------------------------------------------------------------------------------------------------------- 
    // 
    //  PUBLIC & INTERNAL METHODS 
    // 
    //---------------------------------------------------------------------------------------------------------

    //--------------------------------------------------------------------------------------------------------- 
    //
    // PRIVATE & PROTECTED METHODS 
    //
    //---------------------------------------------------------------------------------------------------------
    override protected function _initialize():void{
        _background = new Sprite();
        _background.graphics.beginFill(0xE558F0);
        _background.graphics.drawRect(0,0,60,60);
        _background.graphics.endFill();
        _background.alpha = .7;
        _background.visible = _isSelected;
        addChild(_background);
    }

    private function _crystalImageAdd():void{


        var pStone:Bitmap;

        switch (_crystalVO.type){
            case 0:
                pStone = new Stone1();
                break;
            case 1:
                pStone = new Stone2();
                break;
            case 2:
                pStone = new Stone3();
                break;
            case 3:
                pStone = new Stone4();
                break;
            case 4:
                pStone = new Stone5();
                break;
            default:
                pStone = new Stone5();
        }

        _stone = new Sprite();
        _stone.addChild(pStone);
        _stone.useHandCursor = _stone.buttonMode = true;
        addChild(_stone);

        pTextField = new FlexTextField();
        pTextField.width = pTextField.height = 35;
        var pTextFormat:TextFormat = new TextFormat();
        pTextFormat.bold = true;
        pTextFormat.size = 24;
        pTextField.defaultTextFormat = pTextFormat;
        pTextField.selectable = false;
        addChild(pTextField);
        pTextField.text = _crystalVO.type.toString();
        //temp
        pTextField.visible = false;

        _stone.x = _background.width / 2 -  _stone.width / 2;
        _stone.y = _background.height / 2 -  _stone.height / 2;
    }
    //--------------------------------------------------------------------------------------------------------- 
    // 
    //  EVENT HANDLERS  
    // 
    //---------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------------- 
    // 
    //  END OF CLASS
    // 
    //---------------------------------------------------------------------------------------------------------


}
}
