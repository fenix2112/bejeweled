/**
 * Created with IntelliJ IDEA.
 * User: helga
 * Date: 07.03.14
 * Time: 9:57
 * To change this template use File | Settings | File Templates.
 */
package com.view.components.clock {
import com.view.ViewAbstract;

import flash.events.TimerEvent;

import flash.text.TextField;
import flash.text.TextFormat;
import flash.utils.Timer;

import org.osflash.signals.Signal;

public class Clock extends ViewAbstract{
    //--------------------------------------------------------------------------------------------------------- 
    // 
    //  PUBLIC & INTERNAL VARIABLES 
    // 
    //---------------------------------------------------------------------------------------------------------
    public var signalTimeOver:Signal = new Signal();

    //--------------------------------------------------------------------------------------------------------- 
    //
    // PRIVATE & PROTECTED VARIABLES
    //
    //---------------------------------------------------------------------------------------------------------
    private const _GAME_TIME:int =60;

    private var _textLabel:TextField;
    private var _textTime:TextField;

    private var _timer:Timer;


    //---------------------------------------------------------------------------------------------------------
    //
    //  CONSTRUCTOR 
    // 
    //---------------------------------------------------------------------------------------------------------
    public function Clock() {
    }

    //--------------------------------------------------------------------------------------------------------- 
    // 
    //  GETTERS & SETTERS   
    // 
    //---------------------------------------------------------------------------------------------------------

    //--------------------------------------------------------------------------------------------------------- 
    // 
    //  PUBLIC & INTERNAL METHODS 
    // 
    //---------------------------------------------------------------------------------------------------------

    public function start():void{
        _timer.start();
    }
    //--------------------------------------------------------------------------------------------------------- 
    //
    // PRIVATE & PROTECTED METHODS 
    //
    //---------------------------------------------------------------------------------------------------------
      override protected function _initialize():void{
          _textLabel = new TextField();
          _textTime = new TextField();

          var pTextFormat:TextFormat = new TextFormat();
          pTextFormat.bold = true;
          // pTextFormat.color = 0x0000ff;
          pTextFormat.size = 24;

          _textLabel.width = 100;
          _textLabel.height = 30;
          _textLabel.defaultTextFormat = pTextFormat;
          _textLabel.selectable = false;
          addChild(_textLabel);
          _textLabel.text = "TIME";


          _textTime.width = 100;
          _textTime.height = 30;
          _textTime.defaultTextFormat = pTextFormat;
          _textTime.selectable = false;
          addChild(_textTime);
          _textTime.text = "BLA";

          _textTime.y = _textLabel.y + _textLabel.height + 10;

          _timer = new Timer(1000);
          _timer.addEventListener(TimerEvent.TIMER, _handlerTimer, false, 0, true);
      }
    //--------------------------------------------------------------------------------------------------------- 
    // 
    //  EVENT HANDLERS  
    // 
    //---------------------------------------------------------------------------------------------------------
    private function _handlerTimer(event:TimerEvent):void{
        var pCount:int = _timer.currentCount;

        var pTime:int = (_GAME_TIME - pCount);
        var pTimeText:String;
        if(pTime<10){
            pTimeText = "00:00:0"+(_GAME_TIME - pCount).toString();
        }else{
            pTimeText = "00:00:"+(_GAME_TIME - pCount).toString();
        }

        _textTime.text = pTimeText;
        if(pCount == _GAME_TIME){
            _timer.stop();
            signalTimeOver.dispatch();
        }
    }
    //--------------------------------------------------------------------------------------------------------- 
    // 
    //  END OF CLASS
    // 
    //---------------------------------------------------------------------------------------------------------
}
}
