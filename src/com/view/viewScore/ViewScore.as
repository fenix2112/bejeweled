/**
 * Created with IntelliJ IDEA.
 * User: helga
 * Date: 04.03.14
 * Time: 14:41
 * To change this template use File | Settings | File Templates.
 */
package com.view.viewScore {
import com.view.ViewAbstract;

import flash.display.Bitmap;
import flash.text.TextField;
import flash.text.TextFormat;

import flashx.textLayout.formats.TextAlign;

public class ViewScore extends ViewAbstract {
//--------------------------------------------------------------------------------------------------------- 
    // 
    //  PUBLIC & INTERNAL VARIABLES 
    // 
    //---------------------------------------------------------------------------------------------------------
    [Embed (source="/../src/assets/images/score1.png")]
    private var Score:Class;
    //--------------------------------------------------------------------------------------------------------- 
    //
    // PRIVATE & PROTECTED VARIABLES
    //
    //---------------------------------------------------------------------------------------------------------
    private var _scoreImage:Bitmap;
    private var _scoreText:TextField;
    private var _gameScore:String = "";
    //--------------------------------------------------------------------------------------------------------- 
    //
    //  CONSTRUCTOR 
    // 
    //---------------------------------------------------------------------------------------------------------
    public function ViewScore() {
        super();
    }

    //--------------------------------------------------------------------------------------------------------- 
    // 
    //  GETTERS & SETTERS   
    // 
    //---------------------------------------------------------------------------------------------------------

    public function set gameScore(value:String):void {
        _gameScore = value;
        _scoreText.text = "Your score is: " + _gameScore;
    }
    //--------------------------------------------------------------------------------------------------------- 
    // 
    //  PUBLIC & INTERNAL METHODS 
    // 
    //---------------------------------------------------------------------------------------------------------

    //--------------------------------------------------------------------------------------------------------- 
    //
    // PRIVATE & PROTECTED METHODS 
    //
    //---------------------------------------------------------------------------------------------------------
    override protected function _initialize():void{
        trace(">>> ViewStart");
        _scoreImage = new  Score();
        addChild(_scoreImage);

        _scoreText = new TextField();
         var pTextFormat:TextFormat = new TextFormat();
        pTextFormat.align = TextAlign.CENTER;
        pTextFormat.bold = true;
        pTextFormat.italic = true;
        pTextFormat.size = 48;
        _scoreText.width = 500;
        _scoreText.defaultTextFormat = pTextFormat;
         addChild(_scoreText);

        _scoreImage.x = stage.stageWidth / 2 - _scoreImage.width / 2;
        _scoreImage.y = stage.stageHeight / 3 - _scoreImage.height / 2;

        _scoreText.x =  stage.stageWidth / 2 - _scoreText.width / 2;
        _scoreText.y =  _scoreImage.y + _scoreImage.height + 30;
    }
    //---------------------------------------------------------------------------------------------------------
    // 
    //  EVENT HANDLERS  
    // 
    //---------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------------- 
    // 
    //  END OF CLASS
    // 
    //---------------------------------------------------------------------------------------------------------

}
}
