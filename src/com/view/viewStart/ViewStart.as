/**
 * Created with IntelliJ IDEA.
 * User: helga
 * Date: 04.03.14
 * Time: 14:41
 * To change this template use File | Settings | File Templates.
 */
package com.view.viewStart {
import com.view.ViewAbstract;

import flash.display.Bitmap;

import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.filters.BitmapFilterQuality;

import org.osflash.signals.Signal;

public class ViewStart extends ViewAbstract {
//--------------------------------------------------------------------------------------------------------- 
    // 
    //  PUBLIC & INTERNAL VARIABLES 
    // 
    //---------------------------------------------------------------------------------------------------------
     public var signalStartGame:Signal = new Signal();
    //--------------------------------------------------------------------------------------------------------- 
    //
    // PRIVATE & PROTECTED VARIABLES
    //
    //---------------------------------------------------------------------------------------------------------
    [Embed (source="/../src/assets/images/welcome.jpg")]
    private var Welcome:Class;

    [Embed (source="/../src/assets/images/continue.png")]
    private var ButtonContinue:Class;

    private var _welcomeImage:Bitmap;
    private var _buttonContinue:Sprite;
    //---------------------------------------------------------------------------------------------------------
    //
    //  CONSTRUCTOR 
    // 
    //---------------------------------------------------------------------------------------------------------
    public function ViewStart() {
        super();
    }

    //--------------------------------------------------------------------------------------------------------- 
    // 
    //  GETTERS & SETTERS   
    // 
    //---------------------------------------------------------------------------------------------------------

    //--------------------------------------------------------------------------------------------------------- 
    // 
    //  PUBLIC & INTERNAL METHODS 
    // 
    //---------------------------------------------------------------------------------------------------------

    //--------------------------------------------------------------------------------------------------------- 
    //
    // PRIVATE & PROTECTED METHODS 
    //
    //---------------------------------------------------------------------------------------------------------
     override protected function _initialize():void{
       trace(">>> ViewStart");
         _welcomeImage = new  Welcome();
         addChild(_welcomeImage);

         var pButtonImage:Bitmap = new  ButtonContinue();
         _buttonContinue = new  Sprite();
         _buttonContinue.addChild(pButtonImage);
         _buttonContinue.useHandCursor = _buttonContinue.buttonMode = true;
         addChild(_buttonContinue);

         _buttonContinue.addEventListener(MouseEvent.MOUSE_OVER, _handlerMouseOver, false, 0, true);
         _buttonContinue.addEventListener(MouseEvent.MOUSE_OUT, _handlerMouseOut, false, 0, true);
         _buttonContinue.addEventListener(MouseEvent.CLICK, _handlerMouseClicked, false, 0, true);

         _welcomeImage.x = stage.stageWidth / 2 - _welcomeImage.width / 2;
         _welcomeImage.y = stage.stageHeight / 3 - _welcomeImage.height / 2;

         _buttonContinue.x = stage.stageWidth / 2 - _buttonContinue.width / 2;
         _buttonContinue.y = _welcomeImage.y + _welcomeImage.height + 30;
     }

    //--------------------------------------------------------------------------------------------------------- 
    // 
    //  EVENT HANDLERS  
    // 
    //---------------------------------------------------------------------------------------------------------
    private function _handlerMouseClicked(event:Event):void{
        signalStartGame.dispatch();
    }

    private function _handlerMouseOver(event:Event):void{
        _buttonContinue.alpha = .7;
    }

    private function _handlerMouseOut(event:Event):void{
        _buttonContinue.alpha = 1;
    }
    //--------------------------------------------------------------------------------------------------------- 
    // 
    //  END OF CLASS
    // 
    //---------------------------------------------------------------------------------------------------------
}
}
